create table dcars
(
    rn          serial not null
        constraint dcars_pk
            primary key,
    model       varchar,
    manifacture varchar
);

create
unique index dcars_model_manifacture_uindex
    on dcars (model, manifacture);

create table cars
(
    rn   serial not null
        constraint cars_pk
            primary key,
    drn  integer
        constraint cars_dcars_rn_fk
            references dcars,
    cnum varchar,
    snum varchar
);

create table scars
(
    rn    integer not null
        constraint scar_pk
            primary key
        constraint scar_cars_rn_fk
            references cars,
    lat   double precision,
    lon   double precision,
    fuel  integer,
    event varchar
);

create table dusers
(
    rn    serial  not null
        constraint users_pk
            primary key,
    name  varchar not null,
    lname varchar not null,
    sname varchar,
    code  varchar not null,
    dcode varchar
);

create
index users_name_lname_sname_index
    on dusers (name, lname, sname);

create table users
(
    rn       serial  not null
        constraint users_pk_2
            primary key
        constraint users_dusers_rn_fk
            references dusers,
    login    varchar not null,
    password varchar not null,
    phone    varchar not null
);

create table tariff
(
    rn    serial not null
        constraint tariff_pk
            primary key,
    name  varchar,
    price double precision,
    type  boolean
);

create table history
(
    rn    serial           not null
        constraint history_pk
            primary key,
    stime timestamp        not null,
    etime timestamp        not null,
    slan  double precision not null,
    slon  double precision not null,
    elan  double precision not null,
    elon  double precision not null,
    tlan  double precision,
    tlon  double precision,
    trn   integer          not null
        constraint history_tariff_rn_fk
            references tariff,
    urn   integer          not null
        constraint history_users_rn_fk
            references users,
    crn   integer          not null
        constraint history_cars_rn_fk
            references cars
);

