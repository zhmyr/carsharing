package zhmyr

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import io.circe.syntax.EncoderOps
import org.scalamock.scalatest.MockFactory
import org.scalatest.funspec.AnyFunSpec
import zhmyr.api.CarApi
import zhmyr.logic.CarServiceImpl
import zhmyr.storage.{Car, CarModel}

class CarApiTest extends AnyFunSpec with MockFactory with ScalatestRouteTest {
  describe("GET requests") {
    it("Get near car") {
      Get("/car/nearest?lat=59.94456758741823&lon=30.393346004853854&scale=2") ~> route ~> check {
        assert(status == StatusCodes.OK)
        assert(responseAs[String] == Seq(Car(1, CarModel(1, "Ford", "Focus"), "e2343ew", "123432")).asJson.noSpaces)
      }
    }

    it("Get near car on fake model") {
      Get("/car/nearest?lat=59.94456758741823&lon=30.393346004853854&scale=2&model=3") ~> route ~> check {
        assert(status == StatusCodes.BadRequest)
      }
    }

    it("Get car info") {
      Get("/car/info/1") ~> route ~> check {
        assert(status == StatusCodes.OK)
        assert(responseAs[String] == Car(1, CarModel(1, "Ford", "Focus"), "e2343ew", "123432").asJson.noSpaces)
      }
    }
  }

  describe("Events test") {
    it("motor stop event") {
      Get("/service/motor/1?lat=59.94456758741823&lon=30.393346004853854?fuel=10") ~> route ~> check {
        assert(status == StatusCodes.OK)
        assert(responseAs[String] == "ok")
        for {
          statistic <- db.statisticCar(1L)
        } yield assert(statistic._1 == 59.94456758741823 && statistic._2 == 30.393346004853854 && statistic._3 == 10)
      }
    }

  }

  private val db = new TestStorage
  private val testService = new CarServiceImpl(db)
  private val route = Route.seal(
    new CarApi(testService).router
  )
}
