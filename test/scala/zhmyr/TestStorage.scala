package zhmyr

import slick.jdbc.H2Profile.api._
import slick.jdbc.JdbcBackend.Database
import zhmyr.storage._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class TestStorage extends PostgresStorage {
  override val db = Database.forURL("jdbc:h2:mem:test", driver = "org.h2.Driver", keepAliveConnection = true)

  def statisticCar(carId: Long): Future[(Double, Double, Int)] =
    db.run(statistic.filter(_.id === carId).map(row => (row.lat, row.lon, row.fuel)).result.head)

  db.run(
    (tariffs.schema ++ dictCars.schema ++ cars.schema ++ users.schema ++ dictUsers.schema ++ history.schema ++ statistic.schema).create
      .andThen(dictCars ++= Seq((1, "Ford", "Focus"), (2, "Ford", "Fusion")))
      .andThen(cars ++= Seq((1, 1, "e2343ew", "123432"), (2, 1, "e789wq", "987654"), (3, 2, "e776wq", "987114")))
      .andThen(tariffs ++= Seq((1, "Test", 7.99, true)))
      .andThen(users ++= Seq((1, "user1", "$2a$10$UDowKDR9MQ.xvl/T9kf6/uZhogxz2o9hS5HYDkLO.A6kDoc7KbC1q", "9213447123", "NYDyAFU3Bdh1ZY8l8bfowOB4Knod4Lxz".getBytes)))
      .andThen(dictUsers ++= Seq((1, Some("Иван"), Some("Иванов"), Some("Иванович"), "4012256128", "e213678")))
      .andThen(statistic ++= Seq((1, 59.944444, 30.394167, 30, "motor"), (2, 59.94490611019149, 30.390052252255916, 20, "motor"),
        (3, 59.94851680467672, 30.389483623916252, 40, "motor")))
  )
}