package zhmyr

import akka.http.scaladsl.model.{FormData, StatusCodes}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import io.circe.syntax.EncoderOps
import org.scalamock.scalatest.MockFactory
import org.scalatest.funspec.AnyFunSpec
import zhmyr.api.ClientApi
import zhmyr.logic.ClientServiceImpl
import zhmyr.storage.{Tariff, User}

class ClientApiTest extends AnyFunSpec with MockFactory with ScalatestRouteTest {
  describe("GET requests") {
    it("Tariff") {
      Get("/client/tariff") ~> route ~> check {
        assert(status == StatusCodes.OK)
        assert(responseAs[String].equals(Seq(Tariff(1, "Test", 7.99, timing = true)).asJson.noSpaces))
      }
    }

    it("Get user info") {
      Get("/client/info?token=NYDyAFU3Bdh1ZY8l8bfowOB4Knod4Lxz") ~> route ~> check {
        assert(status == StatusCodes.OK)
        assert(responseAs[String].equals(User(1, "user1", "9213447123", "Иванов Иван Иванович", "4012256128", "e213678").asJson.noSpaces))
      }
    }
  }

  describe("Post requests") {
    var token = "NYDyAFU3Bdh1ZY8l8bfowOB4Knod4Lxz"
    it("New auth by login") {
      Post("/client/login", FormData("login" -> "user1", "password" -> "pooh")) ~> route ~> check {
        assert(responseAs[String] != token)
        token = responseAs[String]
        for {
          u <- db.getUserInfo(token.getBytes)
        } yield assert(u.equals(User(1, "user1", "9213447123", "Иванов Иван Иванович", "4012256128", "e213678")))
      }
    }
    it("New auth by phone") {
      Post("/client/login", FormData("login" -> "9213447123", "password" -> "pooh")) ~> route ~> check {
        assert(responseAs[String] != token)
        token = responseAs[String]
        for {
          u <- db.getUserInfo(token.getBytes)
        } yield assert(u.equals(User(1, "user1", "9213447123", "Иванов Иван Иванович", "4012256128", "e213678")))
      }
    }
  }

  private val db = new TestStorage
  private val testService = new ClientServiceImpl(db)
  private val route = Route.seal(
    new ClientApi(testService).router
  )
}
