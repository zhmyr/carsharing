package zhmyr

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.{Route, RouteConcatenation}
import zhmyr.api.{CarApi, ClientApi}
import zhmyr.logic.{CarServiceImpl, ClientServiceImpl}
import zhmyr.storage.PostgresStorage

import scala.concurrent.ExecutionContext

object SharingApplication {

  implicit val as: ActorSystem = ActorSystem()
  implicit val ac: ExecutionContext = as.dispatcher

  def main(args: Array[String]): Unit = {
    val db = new PostgresStorage
    val clientService = new ClientServiceImpl(db)
    val clientRouter = new ClientApi(clientService)
    val carService = new CarServiceImpl(db)
    val carRouter = new CarApi(carService)
    val router: Route = Route.seal(RouteConcatenation.concat(clientRouter.router, carRouter.router))
    Http().newServerAt("localhost", 8080)
      .bind(router)
      .foreach(s => println(s"server started at $s"))
  }
}