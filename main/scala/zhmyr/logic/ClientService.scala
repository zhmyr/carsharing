package zhmyr.logic

import zhmyr.storage._

import scala.concurrent.{ExecutionContext, Future}

trait ClientService {
  /**
   * Возвращается последовательность тарифов
   *
   * @return Последовательность из case class Tariff
   */
  def tariff: Future[Seq[Tariff]]

  /**
   * Выполняет операцию авторизации
   *
   * @param login    строка с логином или номером телефона
   * @param password проверяемый пароль
   * @return новый токен авторизации или пустую строку при ошибке авторизации
   */
  def login(login: String, password: String): Future[String]

  /**
   * Получение полной информации о пользователе по токену
   *
   * @param token токен авторизации
   * @return соответствующий экземпляр case class User
   */
  def info(token: Array[Byte]): Future[User]

  /**
   * Получение истории заказов по токену для конкретного пользователя и определенным параметрам
   *
   * @param token     токен авторизации
   * @param from      С какой даты отбирать
   * @param to        По какую дату отбирать
   * @param completed Показывать завершенные - 1, незавершенные - 0
   * @param limit     Количество строк в выборке, если не указано, то вернет все
   * @return последовательность из case class HistoryRow
   */
  def history(token: Array[Byte], from: Option[Long], to: Option[Long], completed: Option[Int], limit: Option[Int]): Future[Seq[HistoryRow]]

  /**
   * Создание новой сессии поездки
   *
   * @param token    токен авторизации пользователя
   * @param carId    id машины, которую арендуют
   * @param tariffId id тарифа, по которому едут
   * @return id сессии
   */
  def newOrder(token: Array[Byte], carId: Long, tariffId: Long): Future[Long]

  /**
   * Закрытие сессии
   *
   * @param token   токен авторизации пользователя
   * @param orderId id сессии
   * @param lat     широта
   * @param lon     долгота
   * @return код ошибки (0 в случае успешного завершения)
   */
  def endOrder(token: Array[Byte], orderId: Long, lat: Double, lon: Double): Future[Int]

  /**
   * Обновление координат машины во время сессии
   *
   * @param orderId id сессии
   * @param lat     широта
   * @param lon     долгота
   * @return
   */
  def updateOrder(token: Array[Byte], orderId: Long, lat: Double, lon: Double): Future[Int]
}

class ClientServiceImpl(db: DataStorage)(implicit ec: ExecutionContext) extends ClientService {
  override def tariff: Future[Seq[Tariff]] = db.getAllTariff

  override def login(login: String, password: String): Future[String] = db.getAuthToken(login, password)

  override def info(token: Array[Byte]): Future[User] = db.getUserInfo(token)

  override def history(token: Array[Byte], from: Option[Long], to: Option[Long], completed: Option[Int], limit: Option[Int]): Future[Seq[HistoryRow]] =
    db.getHistoryList(token, from, to, completed, limit)

  override def newOrder(token: Array[Byte], carId: Long, tariffId: Long): Future[Long] =
    db.createOrder(token, carId, tariffId)

  override def updateOrder(token: Array[Byte], orderId: Long, lat: Double, lon: Double): Future[Int] =
    db.updateOrder(token, orderId, lat, lon)

  override def endOrder(token: Array[Byte], orderId: Long, lat: Double, lon: Double): Future[Int] =
    db.endOrder(token, orderId, lat, lon)
}