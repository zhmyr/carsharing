package zhmyr.logic

import zhmyr.storage.{Car, DataStorage}

import scala.concurrent.Future

trait CarService {
  /**
   * Поиск машин в радиусе eps * 100 метров от пользователя
   *
   * @param human координаты пользователя
   * @param eps   n * 100 метров радиус поисковой области
   * @param model последовательностей моделей машиин, которые рассматривает клиент
   * @return последовательность из case class Car
   */
  def closerCars(human: (Double, Double), eps: Int, model: Option[Seq[Long]]): Future[Seq[Car]]

  /**
   * Машина остановилась
   *
   * @param id   ид машины
   * @param lat  широта
   * @param lon  долгота
   * @param fuel уровень топлива
   * @return
   */
  def stopMoving(id: Long, lat: Double, lon: Double, fuel: Int): Future[Int]

  /**
   * Машина заглушила мотор. Сейчас копирует обработчик остановки
   */
  def stopMotorEvent(id: Long, lat: Double, lon: Double, fuel: Int): Future[Int]

  /**
   * Получение информации о машине
   *
   * @param id ид искомой машины
   * @return
   */
  def carInfo(id: Long): Future[Car]
}

class CarServiceImpl(db: DataStorage) extends CarService {
  override def closerCars(human: (Double, Double), eps: Int, model: Option[Seq[Long]]): Future[Seq[Car]] =
    db.getCardsInRaduis(human, eps, model)

  override def stopMotorEvent(id: Long, lat: Double, lon: Double, fuel: Int): Future[Int] =
    db.updateCarStatistic(id, lat, lon, fuel, "motor")

  override def stopMoving(id: Long, lat: Double, lon: Double, fuel: Int): Future[Int] =
    db.updateCarStatistic(id, lat, lon, fuel, "stop")

  override def carInfo(id: Long): Future[Car] = db.getCarById(id, None)
}
