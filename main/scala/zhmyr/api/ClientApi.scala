package zhmyr.api

import akka.actor.ActorSystem
import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{ExceptionHandler, Route}
import io.circe.syntax.EncoderOps
import zhmyr.logic.ClientService

import scala.concurrent.{ExecutionContext, Future}

class ClientApi(service: ClientService)(implicit ac: ExecutionContext, as: ActorSystem) {
  val router: Route =handleExceptions(ExceptionHandler {
    case _ =>
      extractUri { uri =>
        println(s"Request to $uri could not be handled normally")
        complete(HttpResponse(StatusCodes.BadRequest, entity = "Bad numbers, bad result!!!"))
      }
  }) {
    pathPrefix("client") {
      get {
        concat(
          path("tariff") {
            complete(service.tariff.flatMap(seq => Future.successful(seq.asJson.noSpaces)))
          },
          path("info") {
            parameter("token") { str =>
              complete(service.info(str.getBytes).map(_.asJson.noSpaces))
            }
          },
          path("completedOrders") {
            parameters("token", "from".as[Long].optional, "to".as[Long].optional, "completed".as[Int].optional, "limit".as[Int].optional) { (token, f, t, c, l) =>
              complete(service.history(token.getBytes, f, t, c, l).map(_.asJson.noSpaces))
            }
          }
        )
      } ~ post {
        concat(
          path("login") {
            formField("login", "password") { (l, p) =>
              complete(service.login(l, p))
            }
          },
          path("newOrder") {
            formField("token", "car".as[Long], "tariff".as[Long]) { (token, carId, tariffId) =>
              complete(service.newOrder(token.getBytes, carId, tariffId).map(_.asJson.noSpaces))
            }
          },
          path("syncOrder") {
            formField("token", "order".as[Long], "lat".as[Double], "lon".as[Double]) { (t, o, lat, lon) =>
              complete(service.updateOrder(t.getBytes, o, lat, lon).map(_.asJson.noSpaces))
            }
          },
          path("endOrder") {
            formField("token", "order".as[Long], "lat".as[Double], "lon".as[Double]) { (t, o, lat, lon) =>
              complete(service.endOrder(t.getBytes, o, lat, lon).map(_.asJson.noSpaces))
            }
          }
        )
      }
    }
  }
}
