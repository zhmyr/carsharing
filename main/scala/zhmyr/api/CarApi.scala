package zhmyr.api

import akka.actor.ActorSystem
import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{ExceptionHandler, Route}
import io.circe.syntax.EncoderOps
import zhmyr.logic.CarService

import scala.concurrent.ExecutionContext

class CarApi(service: CarService)(implicit ac: ExecutionContext, as: ActorSystem) {

  val router: Route = handleExceptions(ExceptionHandler {
    case _ =>
      extractUri { uri =>
        println(s"Request to $uri could not be handled normally")
        complete(HttpResponse(StatusCodes.BadRequest, entity = "Bad numbers, bad result!!!"))
      }
  }) {
    pathPrefix("service") {
      path("motor" / IntNumber) { n =>
        parameters("lat".as[Double], "lon".as[Double], "fuel".as[Int]) { (lat, lon, fuel) =>
          complete(service.stopMotorEvent(n, lat, lon, fuel).map(_ => "ok"))
        }
      } ~ path("stop" / IntNumber) { n =>
        parameters("lat".as[Double], "lon".as[Double], "fuel".as[Int]) { (lat, lon, fuel) =>
          complete(service.stopMotorEvent(n, lat, lon, fuel).map(_ => "ok"))
        }
      }
    } ~ pathPrefix("car") {
      path("info" / IntNumber) { id =>
        complete(service.carInfo(id).map(_.asJson.noSpaces))
      } ~ path("nearest") {
        parameters("lat".as[Double], "lon".as[Double], "scale".as[Int], "model".as[String].optional) { (lat, lon, eps, model) =>
          complete(service.closerCars((lat, lon), eps, model.map(_.split(",").map(_.toLong))).map(_.asJson.noSpaces))
        }
      }
    }
  }
}
