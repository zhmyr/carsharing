package zhmyr.storage

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

import java.sql.Timestamp
import java.text.SimpleDateFormat
import scala.util.Try

case class CarModel(id: Long, manufacturer: String, model: String)

object CarModel {
  implicit val jsonDecoder: Decoder[CarModel] = deriveDecoder
  implicit val jsonEncoder: Encoder[CarModel] = deriveEncoder
}

case class Car(id: Long, model: CarModel, gosNumber: String, saveNumber: String)

object Car {
  implicit val jsonDecoder: Decoder[Car] = deriveDecoder
  implicit val jsonEncoder: Encoder[Car] = deriveEncoder
}

case class Tariff(id: Long, name: String, price: Double, timing: Boolean)

object Tariff {
  implicit val jsonDecoder: Decoder[Tariff] = deriveDecoder
  implicit val jsonEncoder: Encoder[Tariff] = deriveEncoder
}

case class User(id: Long, login: String, phone: String, fio: String, passport: String, driverLicence: String)

object User {
  implicit val jsonDecoder: Decoder[User] = deriveDecoder
  implicit val jsonEncoder: Encoder[User] = deriveEncoder
}

case class HistoryRow(id: Long, userId: Long, carId: Long, tariffId: Long, startTime: Timestamp, endTime: Timestamp,
                      realEndTime: Option[Timestamp], startCoord: (Double, Double), endCoord: (Double, Double), currentCoord: (Option[Double], Option[Double]))

object HistoryRow {
  implicit val timestampDecoder: Decoder[Timestamp] = Decoder.decodeString.emapTry { str =>
    Try(new Timestamp(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS'Z'").parse(str).getTime))
  }
  implicit val timestampEncoder: Encoder[Timestamp] =
    Encoder.encodeString.contramap[Timestamp](_.formatted("yyyy-MM-dd'T'HH:mm:ss.SS'Z'"))

  implicit val jsonDecoder: Decoder[HistoryRow] = deriveDecoder
  implicit val jsonEncoder: Encoder[HistoryRow] = deriveEncoder
}

case class StatisticPaket(coord: (Double, Double), fuel: Int, error: String = "")

object StatisticPaket {
  implicit val jsonDecoder: Decoder[StatisticPaket] = deriveDecoder
  implicit val jsonEncoder: Encoder[StatisticPaket] = deriveEncoder
}

case class WebMessage(operation: String, lat: Double, lon: Double, context: String)

object WebMessage {
  implicit val jsonDecoder: Decoder[WebMessage] = deriveDecoder
  implicit val jsonEncoder: Encoder[WebMessage] = deriveEncoder
}