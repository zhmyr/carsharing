package zhmyr.storage

import scala.concurrent.Future


trait DataStorage {

  def getAllTariff: Future[Seq[Tariff]]

  def getAuthToken(login: String, pass: String): Future[String]

  def checkAuth(token: Array[Byte]): Future[Boolean]

  def getUserInfo(token: Array[Byte]): Future[User]

  def getHistoryList(token: Array[Byte], from: Option[Long], to: Option[Long], completed: Option[Int], limit: Option[Int]): Future[Seq[HistoryRow]]

  def getCardsInRaduis(human: (Double, Double), num: Int, model: Option[Seq[Long]]): Future[Seq[Car]]

  def getCarById(id: Long, model: Option[Seq[Long]]): Future[Car]

  def updateCarStatistic(id: Long, lat: Double, lon: Double, fuel: Int, event: String): Future[Int]

  def createOrder(token: Array[Byte], carId: Long, tariffId: Long): Future[Long]

  def updateOrder(token: Array[Byte], orderId: Long, lat: Double, lon: Double): Future[Int]

  def endOrder(token: Array[Byte], orderId: Long, lat: Double, lon: Double): Future[Int]
}