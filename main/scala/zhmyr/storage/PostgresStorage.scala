package zhmyr.storage

import com.github.t3hnar.bcrypt.BCryptStrOps
import slick.jdbc.H2Profile.api._
import slick.jdbc.JdbcBackend.Database
import slick.lifted.{ForeignKeyQuery, ProvenShape}

import java.sql.Timestamp
import java.time.Instant
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Random, Success}

class PostgresStorage(implicit ac: ExecutionContext) extends DataStorage {
  val alpha = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
  val rad = 0.000899872 // 100 m in 1 deg
  val dictCars = TableQuery[DictCarsTable]
  val cars = TableQuery[CarsTable]
  val tariffs = TableQuery[TariffTable]
  val users = TableQuery[UsersTable]
  val dictUsers = TableQuery[DictUsersTable]
  val history = TableQuery[HistoryTable]
  val statistic = TableQuery[CarStatisticTable]
  val db = Database.forURL("jdbc:postgresql://192.168.33.10/market?user=market&password=123456", driver = "org.postgresql.Driver")

  override def getAllTariff: Future[Seq[Tariff]] = for {
    t <- db.run(tariffs.result)
  } yield t.map(row => Tariff(row._1, row._2, row._3, row._4))

  override def getAuthToken(login: String, pass: String): Future[String] = for {
    Some((id, pas)) <- db.run(
      users.filter(row => row.login === login || row.phone === login)
        .map(row => (row.id, row.hash))
        .result.headOption)
    valid <- Future {
      pass.isBcrypted(pas)
    }
      .transformWith {
        case Success(valid) => Future.successful(if (valid) randStr(32) else "")
        case Failure(_) => Future.successful("")
      }
    _ <- db.run(users.filter(_.id === id).map(_.token).update(valid.getBytes).transactionally) if valid.nonEmpty
  } yield valid

  private def randStr(n: Int): String = (1 to n).map(_ => alpha(Random.nextInt(alpha.length))).mkString

  def getHistoryList(token: Array[Byte], from: Option[Long], to: Option[Long], completed: Option[Int], limit: Option[Int]): Future[Seq[HistoryRow]] = for {
    user <- db.run(users.filter(_.token === token).result.head)
    hist <- db.run(history.filter(_.userId === user._1)
      .filterOpt(from)(_.startTimestamp >= new Timestamp(_)).filterOpt(to)(_.endTimestamp <= new Timestamp(_))
      .filterIf(completed.isDefined)(_.realEndTimestamp.isDefined && completed.get == 1).result)
  } yield hist.take(limit.getOrElse(hist.length))
    .map(row => HistoryRow(row._1, row._3, row._4, row._2, row._5, row._6.get, row._7, (row._8, row._9), (row._10.get, row._11.get), (row._12, row._13)))

  override def getCardsInRaduis(human: (Double, Double), num: Int, model: Option[Seq[Long]]): Future[Seq[Car]] = for {
    coord <- db.run(statistic.filter(_.fuel > 5)
      .map(row => (row.id, row.lat, row.lon)).result)
    ids <- Future.successful(coord.filter(row => Math.sqrt(Math.pow(human._1 - row._2, 2) + Math.pow(human._2 - row._3, 2)) <= num * rad).map(_._1))
    cars <- Future.sequence(ids.foldLeft(Seq.empty[Future[Car]])((f, l) => f :+ getCarById(l, model)))
  } yield cars

  override def getCarById(id: Long, model: Option[Seq[Long]]): Future[Car] = for {
    Some(c) <- db.run(cars.join(dictCars).on((c, d) => c.modelId === d.id && c.id === id)
      .filterOpt(model)((tbl, opt) => tbl._2.id.inSet(opt))
      .map(q => (q._1.id, q._2.id, q._2.manufacturer, q._2.model, q._1.gosNumber, q._1.saveNumber))
      .result.headOption)
  } yield Car(c._1, CarModel(c._2, c._3, c._4), c._5, c._6)

  override def updateCarStatistic(id: Long, lat: Double, lon: Double, fuel: Int, event: String): Future[Int] =
    db.run(statistic.filter(_.id === id).map(row => (row.lat, row.lon, row.fuel))
      .update(lat, lon, fuel).transactionally)

  override def createOrder(token: Array[Byte], carId: Long, tariffId: Long): Future[Long] =
    db.run(sql"SELECT count(rn) FROM history where crn = $carId and elan is null".as[Int].head).flatMap(c => {
      if (c == 0) {
        for {
          uid <- getUserInfo(token).map(_.id)
          Some((lat, lon)) <- db.run(sql"SELECT lat, lon from scars where rn = $carId".as[(Double, Double)].headOption)
          lid <- db.run(sql"insert into history (urn, crn, trn, slan, slon, stime) values ($uid, $carId, $tariffId, $lat, $lon, now()) returning rn".as[Long].head)
        } yield lid
      } else Future.successful(-1)
    })

  //  {
  //    db.run(history.filter(row => row.carId === carId && row.tempLongitude.isEmpty).sortBy(_.tempLongitude.nullsFirst).map(_.tempLongitude).result.headOption).flatMap { row =>
  //      if (row.isEmpty || row.flatten.isEmpty) {
  //        val halfHistory = history.map(row => (row.id, row.userId, row.tariffId, row.carId, row.startLatitude, row.startLongitude, row.startTimestamp))
  //        for {
  //          Some(uid) <- db.run(users.filter(_.token === token).map(_.id).result.headOption)
  //          Some(coord) <- db.run(statistic.filter(_.id === carId).map(row => (row.lat, row.lon)).result.headOption)
  //          Some(lastId) <- db.run(((halfHistory returning halfHistory.map(_._1))
  //            += (None, uid, tariffId, carId, coord._1, coord._2, Timestamp.from(Instant.now()))).transactionally)
  //        } yield lastId
  //      } else {
  //        Future.successful(-1)
  //      }
  //    }
  //  }

  override def getUserInfo(token: Array[Byte]): Future[User] = {
    checkAuth(token).flatMap(valid =>
      if (valid)
        db.run(for {
          Some((u, d)) <- users.filter(_.token === token).join(dictUsers).on(_.id === _.id).result.headOption
        } yield User(u._1, u._2, u._4, Seq(d._3, d._2, d._4).flatten.mkString(" "), d._5, d._6))
      else
        Future.failed(new IllegalStateException())
    )
  }

  override def checkAuth(token: Array[Byte]): Future[Boolean] =
    db.run(users.filter(_.token === token).result).map(_.nonEmpty)

  override def updateOrder(token: Array[Byte], orderId: Long, lat: Double, lon: Double): Future[Int] =
    for {
      uid <- getUserInfo(token)
      upd <- db.run(history.filter(row => row.id === orderId && row.endLatitude.isEmpty && row.userId === uid.id).map(row => (row.tempLatitude, row.tempLongitude)).update((Some(lat), Some(lon))).transactionally)
    } yield -1 + upd

  override def endOrder(token: Array[Byte], orderId: Long, lat: Double, lon: Double): Future[Int] =
    checkAuth(token).flatMap(login => {
      if (login) {
        val validUser = for {
          Some(uid) <- db.run(users.filter(_.token === token).map(_.id).result.headOption)
          order <- db.run(history.filter(row => row.id === orderId && row.userId === uid).result.headOption)
        } yield order.isDefined
        validUser.flatMap(vu => {
          if (vu) {
            val validCoord = for {
              Some(cid) <- db.run(history.filter(_.id === orderId).map(_.carId).result.headOption)
              Some((clat, clon)) <- db.run(statistic.filter(row => row.id === cid && row.event === "motor").map(row => (row.lat, row.lon)).result.headOption)
              res <- Future.successful(Math.sqrt(Math.pow(lat - clat, 2) + Math.pow(lon - clon, 2)) < (rad * 0.1))
            } yield res
            validCoord.flatMap { valid =>
              if (valid) {
                db.run(history.filter(_.id === orderId).map(row => (row.endLatitude, row.endLongitude, row.endTimestamp))
                  .update((Some(lat), Some(lon), Some(Timestamp.from(Instant.now())))).transactionally)
                Future.successful(0)
              } else
                Future.successful(-3)
            }
          } else Future.successful(-2)
        })
      } else Future.successful(-1)
    })

  class DictCarsTable(tag: Tag) extends Table[(Long, String, String)](tag, "dcars") {
    override def * : ProvenShape[(Long, String, String)] = (id, manufacturer, model)

    def id: Rep[Long] = column("rn", O.PrimaryKey, O.AutoInc)

    def manufacturer: Rep[String] = column("manuf")

    def model: Rep[String] = column("model")
  }

  class CarsTable(tag: Tag) extends Table[(Long, Long, String, String)](tag, "cars") {
    def model: ForeignKeyQuery[DictCarsTable, (Long, String, String)] =
      foreignKey("model_fk", modelId, dictCars)(_.id)

    def modelId: Rep[Long] = column("drn")

    override def * : ProvenShape[(Long, Long, String, String)] = (id, modelId, gosNumber, saveNumber)

    def id: Rep[Long] = column("rn", O.PrimaryKey, O.AutoInc)

    def gosNumber: Rep[String] = column("gnum")

    def saveNumber: Rep[String] = column("snum")
  }

  class TariffTable(tag: Tag) extends Table[(Long, String, Double, Boolean)](tag, "tariff") {
    override def * = (id, name, price, mode)

    def id: Rep[Long] = column("rn", O.PrimaryKey, O.AutoInc)

    def name: Rep[String] = column("name")

    def price: Rep[Double] = column("price")

    def mode: Rep[Boolean] = column("type")
  }

  class UsersTable(tag: Tag) extends Table[(Long, String, String, String, Array[Byte])](tag, "users") {
    def * = (id, login, hash, phone, token)

    def id: Rep[Long] = column("rn", O.PrimaryKey, O.AutoInc)

    def login: Rep[String] = column("login")

    def hash: Rep[String] = column("password")

    def phone: Rep[String] = column("phone")

    def token: Rep[Array[Byte]] = column("token")
  }

  class DictUsersTable(tag: Tag) extends Table[(Long, Option[String], Option[String], Option[String], String, String)](tag, "dusers") {
    def * = (id, name, lname, sname, passport, license)

    def id: Rep[Long] = column("rn", O.PrimaryKey, O.AutoInc)

    def name: Rep[Option[String]] = column("name")

    def lname: Rep[Option[String]] = column("lname")

    def sname: Rep[Option[String]] = column("sname")

    def passport: Rep[String] = column("code")

    def license: Rep[String] = column("dcode")
  }

  class HistoryTable(tag: Tag) extends Table[(Long, Long, Long, Long, Timestamp, Option[Timestamp], Option[Timestamp], Double, Double, Option[Double], Option[Double], Option[Double], Option[Double])](tag, "history") {
    def * = (id, tariffId, userId, carId, startTimestamp, endTimestamp, realEndTimestamp, startLatitude, startLongitude, endLatitude, endLongitude, tempLatitude, tempLongitude)

    def id: Rep[Long] = column("rn", O.PrimaryKey, O.AutoInc)

    def tariffId: Rep[Long] = column("trn")

    def userId: Rep[Long] = column("urn")

    def carId: Rep[Long] = column("crn")

    def startTimestamp: Rep[Timestamp] = column("stime")

    def realEndTimestamp: Rep[Option[Timestamp]] = column("rtime")

    def endTimestamp: Rep[Option[Timestamp]] = column("etime")

    def startLatitude: Rep[Double] = column("slan")

    def startLongitude: Rep[Double] = column("slon")

    def endLatitude: Rep[Option[Double]] = column("elan")

    def endLongitude: Rep[Option[Double]] = column("elon")

    def tempLatitude: Rep[Option[Double]] = column("tlan")

    def tempLongitude: Rep[Option[Double]] = column("tlon")
  }

  class CarStatisticTable(tag: Tag) extends Table[(Long, Double, Double, Int, String)](tag, "scars") {
    def * = (id, lat, lon, fuel, event)

    def id: Rep[Long] = column("rn", O.PrimaryKey, O.AutoInc)

    def lat: Rep[Double] = column("lat")

    def lon: Rep[Double] = column("lon")

    def fuel: Rep[Int] = column("fuel")

    def event: Rep[String] = column("event")
  }

}